//services
const servicesPhoto = document.querySelectorAll(".services__information-photo")
const servicesText = document.querySelectorAll('.services__information-text')
const navLiServices = document.querySelectorAll('.services__nav-a')

document.querySelector('.services__nav-list').addEventListener("click", event => {
    if (event.target.tagName !== "LI") return false;
    let filterClass = event.target.dataset['name'];

    navLiServices.forEach(item => {
        item.classList.remove('services__nav-a-active')
        event.target.classList.add('services__nav-a-active')
    })
    servicesPhoto.forEach(elem => {
        elem.classList.remove('hide')
        if (!elem.classList.contains(filterClass)) {
            elem.classList.add('hide')
            
        }
    })
    servicesText.forEach(elem => {
        elem.classList.remove('hide')
        if (!elem.classList.contains(filterClass)) {
            elem.classList.add('hide')
        }
    })
})
//works
const worksPhoto = document.querySelectorAll(".works__images-image")
const navLiWorks = document.querySelectorAll('.works__nav-li');
const worksHover = document.querySelectorAll('.works__images-text')
let worksPhotoActive = document.querySelectorAll(".works__images-image-active")
const buttonAddMore = document.querySelector('.works-button');
const loader = document.querySelector('.circle')

document.querySelector('.works__nav-list').addEventListener("click", event => {
    if (event.target.tagName !== "LI") return false;

    let filterClass = event.target.dataset['f'];

    navLiWorks.forEach(item => {
        item.classList.remove('works__nav-li-active')
        event.target.classList.add('works__nav-li-active')
    })

    worksPhoto.forEach(elem => {
        elem.classList.remove('hide')
        if (!elem.classList.contains(filterClass)) {
            elem.classList.add('hide')
        }
    })
})

function workAddPhotos() {
    setTimeout(uploadingPhotos, 2000)
}

function uploadingPhotos() {
    let worksPhotoActive = document.querySelectorAll('.works__images-image-active')
    
    if (worksPhotoActive.length === 12) {
        for (let i = 12; i <= 23; i++) {
            worksPhoto[i].classList.add('works__images-image-active')
        }
    }else if (worksPhotoActive.length === 24) {
        for (let i = 24; i <= 35; i++) {
            worksPhoto[i].classList.add('works__images-image-active')
        }
        buttonAddMore.style.visibility = "hidden"
    }
}

 function circleLoad() {
    setTimeout(() => {loader.style.opacity = '1'}, 0);
    setTimeout(() => {loader.style.opacity = '0'}, 2000)
}

buttonAddMore.addEventListener('click', workAddPhotos)
buttonAddMore.addEventListener('click', circleLoad)

//people
const comments = document.querySelectorAll('.slider-item');
const sliderImg = document.querySelectorAll('.people-img');

const leftBtn = document.querySelector('.people__button-left');
const rightBtn = document.querySelector('.people__button-right');
let current = 0;

function slider(){
 for (let i = 0; i < sliderImg.length; i++) {
  sliderImg[i].classList.remove('people-img-up');
 }
 sliderImg[current].classList.add('people-img-up');

 for(let j = 0; j < comments.length; j++){
  comments[j].classList.remove('active');
 }
 comments[current].classList.add('active');
}
slider();

leftBtn.addEventListener('click',()=>{
 if(current - 1 === - 1){
  current = sliderImg.length - 1;
 }else{
  current--;
 }
 slider();
})

rightBtn.addEventListener('click', ()=>{
 if(current + 1 === sliderImg.length){
  current = 0;
 }else{
  current++;
 }
 slider();
 
})