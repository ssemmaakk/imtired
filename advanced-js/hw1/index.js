//Обьясните своими словами, как вы понимаете, как работает прототипное наследование в Javascript
//Когда мы создаем прототип какого-то обьекта мы можем получить свойства для этого обьекта из обьекта с которого мы сделали прототип

class Employee {
    constructor(name,age,salary){
        this._name = name
        this._age = age
        this._salary = salary
    }
    set name(value) {
        this._name = value
        return this._name
    }
    set age(value) {
        this._age = value
        return this._age
    }
    set salary(value) {
        this._age = value
        return this._salary
    }
    get name(){
        return this._name
    }
    get age(){
        return this._age
    }
    get salary(){
        return this._salary
    }
}

class Programmer extends Employee {
    constructor(name,age,salary,lang){
        super(name,age,salary)
        this._lang = lang
    }
    get salary(){
        return `Зарплата работника ${this._name} ${this._salary * 3 }`
    }
}

const vlad = new Programmer('Vlad',23, 1000,'html css')
console.log(vlad);
console.log(vlad.salary);

const eugene = new Programmer('Eugene',33, 4000,'js react')
console.log(eugene);
console.log(eugene.salary);

const egor = new Programmer('Egor',40, 5000,'js vue nuxt')
console.log(egor);
console.log(egor.salary);