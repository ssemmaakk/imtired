const books = [
    {
        author: "Скотт Бэккер",
        name: "Тьма, что приходит прежде",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Воин-пророк",
    },
    {
        name: "Тысячекратная мысль",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Нечестивый Консульт",
        price: 70
    },
    {
        author: "Дарья Донцова",
        name: "Детектив на диете",
        price: 40
    },
    {
        author: "Дарья Донцова",
        name: "Дед Снегур и Морозочка",
    }
];
const block = document.getElementById('root')
books.forEach(item => {
    try {
        if (!item.hasOwnProperty('author')) {
            throw new SyntaxError(item + " don't have a author")
        }
        if (!item.hasOwnProperty('name')) {
            throw new SyntaxError(item + " don't have a name")
        }
        if (!item.hasOwnProperty('price')) {
            throw new SyntaxError(item + " don't have a price")
        }
        let currentBook = document.createElement('ul')
        let bookProperty = document.createElement('li')

        block.append(currentBook)
        currentBook.appendChild(bookProperty)
        bookProperty.innerText = `название:${item.name} автор:${item.author} цена:${item.price}`
            } catch (err) {
        console.log(err.message)
    }
})
