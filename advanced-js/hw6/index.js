//с помощью асинхронности мы можем выполнять долгие сетевые запросы не блокировая текущий поток

const btn = document.getElementById('findPerson')
btn.addEventListener('click', getPerson)

async function getPerson(){
  try {
    const response1 = await fetch('http://api.ipify.org/?format=json')
    const data1 = await response1.json()

    const url = `http://ip-api.com/json/${data1.ip}`
    const response2 = await fetch(url)
    const data2 = await response2.json()
    const { timezone, country, regionName, city, region } = data2

   document.body.insertAdjacentHTML( "beforeend", `
    <ul>
        <li>Континент: ${timezone}</li>
        <li>Страна: ${country}</li>
        <li>Регион: ${regionName}</li>
        <li>Город: ${city}</li>
        <li>Район города: ${region}</li>
    </ul>
   `)
  }catch(err){
    console.error('Error',err)
  }
}
