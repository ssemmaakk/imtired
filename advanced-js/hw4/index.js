const sendRequest = (type, url) => {
    return new Promise((resolve, reject) => {
        const request = new XMLHttpRequest()
        request.open(type, url)
        request.send()
        request.responseType = 'json'
        request.onload = function () {
            resolve(request.response)
        }
    })
}

async function dataOutput(type, url) {
    const requestReceived = sendRequest(type, url)
    requestReceived.then(async movies => {
        const data = await Promise.all(movies.map(async movie => {
            return [movie, await Promise.all(movie.characters.map(character => sendRequest("GET", character)))]
        }))
        display(data)
    })
}

function display(data) {
    document.querySelector('.loader').remove()
    for (let episodeData of data) {
        const episode = episodeData[0];
        let list = document.createElement('ul')
        document.body.append(list)
        list.append(`Episode ${episode.episodeId}`)
        let characters = episodeData[1];
        for (let character of characters) {
            let filmItem = document.createElement('li')
            filmItem.innerText = character.name
            list.append(filmItem)
        }
        let filmItem = document.createElement('li')
        filmItem.innerText = episode.name
        list.append(`Episod name: ${filmItem.innerText}`)
        filmItem = document.createElement('li')
        filmItem.innerText = episode.openingCrawl
        list.append(filmItem)
    }
}

const request = dataOutput('GET', 'https://ajax.test-danit.com/api/swapi/films')
