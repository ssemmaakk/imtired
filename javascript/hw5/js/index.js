
function createNewUser () {
    let newUser = {
        _firstName: prompt('Enter your first name'),
        _lastName: prompt('Enter your last name'),
        birthday: prompt('Enter your birth day'),
        getLogin: function() {
            return this._firstName[0].toLowerCase() +
                this._lastName.toLowerCase()
        },
      
        getAge() {
            let bDate  = new Date(this.birthday.split(".").reverse().join('-'));
           return Math.floor( ( (new Date() - bDate) / 1000 / (60 * 60 * 24) ) / 365.25 );
        },
        getPassword: function () {
            return this._firstName[0].toUpperCase() +
                this._lastName.toLowerCase() + this.getAge();
        },
    }
    return `    Your login: ${newUser.getLogin()}
    Your password: ${newUser.getPassword()}
    Your age: ${newUser.getAge()}`

}
console.log(createNewUser());