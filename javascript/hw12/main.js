const img = document.querySelectorAll('.image-to-show')
const div = document.querySelector('.images-wrapper')
const btnPause = document.createElement('button')
const btnReturn = document.createElement('button')

btnPause.innerHTML = 'Прекратить'
btnPause.classList.add('btn')
btnReturn.innerHTML = 'Возобновить показ'
btnReturn.classList.add('btn')
document.body.append(btnPause, btnReturn)

img.forEach(function (element) {
    element.style.display = 'none'
})
img[0].style.display ='block'
let c = 0

function change() {

    img[c].style.display = 'none'
    c += 1

    if (c === img.length) {
        c = 0
    }
    img[c].style.display = 'block'

}

let ti = setInterval(change, 3000)

btnPause.addEventListener('click', function () {
    clearInterval(ti)

})

btnReturn.addEventListener('click', function () {
    clearInterval(ti)
    ti = setInterval(change, 3000)

})
