const link = document.querySelector('#shtylesheet')
const btn = document.getElementById('btn')
const light = 'css/style.css'
const dark = 'css/theme1.css'

const changeTheme = () => {
     const href = link.getAttribute('href')
    if (href === 'css/theme1.css') {
        link.setAttribute('href', light)
        localStorage.setItem('theme', light)
    } else {
        link.setAttribute('href', dark)
        localStorage.setItem('theme', dark)
    }
}

btn.addEventListener('click', changeTheme)

const checkTheme = () => {
    const storageTheme = localStorage.getItem('theme')
    if(storageTheme && storageTheme === dark){
        link.setAttribute('href', dark)
        localStorage.setItem('theme', dark)
    }
}

window.onload = checkTheme()