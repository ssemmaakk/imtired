
let input = document.querySelector('input');
let list = document.querySelector('.price-list');
let alert = document.createElement("span")

input.setAttribute('type', 'number')

function createPrice(event) {
    const inputValue = input.value;
    if(inputValue < 0 ){
        const alert = document.createElement("span")
        alert.innerText = 'Please enter correct price'
        alert.style.color = 'red'
        document.body.append(alert)   
        input.style.outline = '2px solid red'
        //Как сделать чтобы оно один раз алерт выводило и удалялось после введения правильного числа?
    }else if (event.key === 'Enter' && inputValue.length) {
        let listElement = document.createElement('li');
        listElement.classList.add('item');
        listElement.innerText = `Текущая цена: ${inputValue}$`;
        input.value = '';
        input.style.borderColor = 'green'

        const deleteButton = document.createElement('button');
        deleteButton.innerText = 'X';
        deleteButton.classList.add('deleteButton');

        list.append(listElement);
        listElement.append(deleteButton);
        
    }

}


input.addEventListener('focus' , (e) => {
    input.style.outline = "2px solid green"
});
input.addEventListener('blur' , () => {
    input.style.outline = "1px solid black"
});
input.addEventListener('keydown', createPrice);

list.addEventListener('click', event => {
    if (event.target.tagName === 'BUTTON' && event.target.classList.contains('deleteButton')) {
        event.target.parentNode.remove();
    }
})

